/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba;

/**
 *
 * @author Laboratorios
 */
public class Prueba {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String adn = "GGACT";
        
        int edadMAX = (contarCHARS(adn, 'C') + 1) * (contarCHARS(adn, 'G') + 1) + (contarCHARS(adn, 'T') + 1) * 10;
        
        System.out.println(edadMAX);
    }
    
    public static int contarCHARS(String adn, char nucleotide) {
        int count = 0;
        for (int i = 0; i < adn.length(); i++) {
            if (adn.charAt(i) == nucleotide) {
                count++;
            }
        }
        return count;
    }
    
}
